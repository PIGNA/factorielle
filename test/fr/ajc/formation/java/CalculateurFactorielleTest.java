package fr.ajc.formation.java;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalculateurFactorielleTest {

	@Test
	void VerifyFactorial3Equals6() {
		assertEquals(6, CalculateurFactorielle.factorielle(3));
	}
	
	@Test
	void VerifyFactorial4Equals24() {
		assertEquals(24,CalculateurFactorielle.factorielle(4));
	}

}
